*** Settings ***
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables         var/variables.py
Resource          create-account.robot
Resource          login.robot
Resource          base-setup.robot


*** Test Cases ***
Launching FacePay Application
    Log    Open Application       
    Launch FacePay Application
     
Create Account shall Fail
    Log    Testing Create account creation failing
    Create Account Should Fail    
    
 Create Account shall Pass
     Log    Testing Create account successfull creation
       
     Relaunch Application
     Create Account Should Pass  
     
 Login shall fail
     Log    Testing Login to application for failing     
     Relaunch Application
     Login Should Fail
     
 Login shall Pass
    Log    Testing Login to application for successfull
    Relaunch Application
    Login Should Pass
      
    

   
 
