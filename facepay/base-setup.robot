*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    var/variables.py


*** Keywords ***

Relaunch Application
    Close Application
    Sleep    2s    
    Launch FacePay Application
        

# SMS App
Open SMS
    Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=9    deviceName=f0a4c17c    appPackage=com.android.mms    appActivity=com.android.mms.ui.ConversationList
    Sleep    5s 

# FacePay  Application
Launch FacePay Application
   Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=9    deviceName=f0a4c17c    appPackage=com.rncameraexample    appActivity=com.rncameraexample.MainActivity
   Sleep    5s