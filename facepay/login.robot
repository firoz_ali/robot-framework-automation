*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    var/variables.py


*** Keywords ***

Login Should Fail
      
      Sleep    2s
      Page Should Contain Text   ${LoginTitle}
    
      Input Text   accessibility_id=email           ${WrongEmail}
      Input Text   accessibility_id=password        ${WrongPassword}
      Sleep    2s
      
      Click Element    accessibility_id=login
      Sleep    5s 
      
      ${toast}     Get Text    accessibility_id=toast 
        
      Should Not Be Equal    ${toast}    None    
      Log    ${toast}  
      Sleep    2s
      
   
   
Login Should Pass
    
      Sleep    2s    
      Page Should Contain Text    ${LoginTitle}
    
      Input Text   accessibility_id=email           ${Email}
      Input Text   accessibility_id=password        ${Password}
      Sleep    5s
      Click Element    accessibility_id=login
      Sleep    5s 
      
      Wait Until Page Contains    ${patternTitle}
      

        

    

   
   