def get_variables(arg=None):
    variables = {
        'var' : 'value',
        'LoginTitle' : u'Welcome to FacePay!',
        'CreateAccountTitle' :     u'Create an account',
        'ValidateTitle'      :     u'Validate OTP',
        'patternTitle':            u'Draw an unlock pattern',
        'AppName':                 u'FacePay',
        'Name':                    u'Test User',
        'Email':                   u'sh.a.r.p.t.h.ee.@gmail.com',
        'Password':                u'123456',
        'PhoneNumber':             u'8660103611',
        'NewName':                 u'New Test User',
        'NewEmail':                u's..h.a.r.p.t.h.e.e.@gmail.com',
        'NewPassword':             u'123456',
        'NewPhoneNumber':          u'8660103611',  
        'WrongEmail':              u'sh.a.r.p.t.h.ee.@gmail.com',
        'WrongPassword':           u'1234567'  
        }
    return variables
